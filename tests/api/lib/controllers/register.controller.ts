import { ApiRequest } from "../request";

export class RegisterController {
    async register(email: string, userName: string, password: string) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Register`)
            .body({
                id: 0,
                avatar: "default_avatar",
                email: email,
                userName: userName,
                password: password,
            })
            .send();
        return response;
    }
}