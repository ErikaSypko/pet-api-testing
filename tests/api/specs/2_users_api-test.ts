import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";

const users = new UsersController();
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe(`Users controller`, () => {
    let userId: number;
 
    it(`should return 200 status code and all users when getting the user collection`, async () => {
        let response = await users.getAllUsers();

        // console.log("All Users:");
        // console.log(response.body);

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 1s`).to.be.lessThan(1000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1); 
        expect(response.body).to.be.jsonSchema(schemas.schema_allUsers); 
        
        userId = response.body[1].id;
    });

    describe("Get User Details by ID", () => {
        it("should return details of a specific user by ID", async () => {
            let userId = 7111

            const response = await users.getUserById(userId);
    
            expect(response.statusCode, "Status Code should be 200").to.be.equal(200);
        });
    });

    it(`should return 404 error when getting user details with invalid id`, async () => {
        let invalidUserId = 123133

        let response = await users.getUserById(invalidUserId);

        expect(response.statusCode, `Status Code should be 404`).to.be.equal(404);
        expect(response.timings.phases.total, `Response time should be less than 1s`).to.be.lessThan(1000);  
    });

    it(`should return 400 error when getting user details with invalid id type`, async () => {
        let invalidUserId = '2183821367281387213781263'

        let response = await users.getUserById(invalidUserId);

        expect(response.statusCode, `Status Code should be 400`).to.be.equal(400);
        expect(response.timings.phases.total, `Response time should be less than 1s`).to.be.lessThan(1000);  
    });

    it(`should return user details when getting user details with valid id`, async () => {
        let response = await users.getAllUsers();
        let firstUserId: number = response.body[0].id;
        
        response = await users.getUserById(firstUserId);

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 1s`).to.be.lessThan(1000); 

        // console.log(response.body);
    });

    it("should delete the currently logged-in user", async () => {
        const usersID = 7112;
        const usersToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6IkpXVCJ9.eyJzdWIiOiJzdHJpbmciLCJlbWFpbCI6InN0cmluZ0BnbWFpbC5jb20iLCJqdGkiOiI1NTAyN2Y5MS1mMTE5LTQyOTgtYWRjMy1mNTQ4NjI2Zjc4ZjUiLCJpYXQiOjE2OTE5NjY4NzAsImlkIjoiNzExMiIsIm5iZiI6MTY5MTk2Njg2OSwiZXhwIjoxNjkxOTc0MDY5LCJpc3MiOiJUaHJlYWQuTkVUIFdlYkFQSSIsImF1ZCI6Imh0dHBzOi8vbG9jYWxob3N0OjQ0MzQ0In0.D2uBQiV0cpJSfWF_QpCw7CFg1f7swjkPfE9rdNV-OOE'

        const response = await users.deleteUserById(usersID, usersToken);
    
        expect(response.statusCode, "Status Code should be 204").to.be.equal(204);
    });



    it("should successfully get current user details based on the token", async () => {
        let usersToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6IkpXVCJ9.eyJzdWIiOiJlcmlrYXN5cGtvIiwiZW1haWwiOiJlcmlrYXN5cGtvQGV4YW1wbGUuY29tIiwianRpIjoiM2UzYTJhNWYtZGM1YS00ZWFiLWFiZDAtMTNjODU2MTg3MjRkIiwiaWF0IjoxNjkxOTYyODAxLCJpZCI6IjcxMTEiLCJuYmYiOjE2OTE5NjI4MDEsImV4cCI6MTY5MTk3MDAwMSwiaXNzIjoiVGhyZWFkLk5FVCBXZWJBUEkiLCJhdWQiOiJodHRwczovL2xvY2FsaG9zdDo0NDM0NCJ9.LZD2XdZkTZgbyXPVkzTmJo1HuT52KIM3UtR_zsxk__8'

        const currentUserFromToken = await users.getCurrentUserDetailsFromToken(usersToken);
        const email = "erikasypko@example.com";
        const userName = "erikasypko";
        const avatar = "default_avatar"
        
        expect(currentUserFromToken.statusCode).to.equal(200);
        expect(currentUserFromToken.body).to.have.property("avatar").to.equal(avatar);
        expect(currentUserFromToken.body).to.have.property("email").to.equal(email);
        expect(currentUserFromToken.body).to.have.property("userName").to.equal(userName);
    });


});
