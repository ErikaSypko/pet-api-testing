// import { expect } from "chai";
// import { RegisterController } from "../lib/controllers/register.controller";

// describe("User Registration", () => {
//     let regController;

//     before(() => {
//         regController = new RegisterController();
//     });

//     it("should successfully register a new user", async () => {
//         const email = "erikasypko@example.com";
//         const userName = "erikasypko";
//         const password = "password123";

//         const registrationResponse = await regController.register(email, userName, password);

//         expect(registrationResponse.statusCode).to.equal(200);
//         expect(registrationResponse.body).to.have.property("id").that.is.a("number");
//         expect(registrationResponse.body).to.have.property("avatar").that.is.a("string");
//         expect(registrationResponse.body).to.have.property("email").to.equal(email);
//         expect(registrationResponse.body).to.have.property("userName").to.equal(userName);
//     });
// });