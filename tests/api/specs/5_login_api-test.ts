import { expect } from "chai";
import { AuthController } from "../lib/controllers/auth.controller";

const auth = new AuthController();

describe("User Login", () => {
    let accessToken: string;

    before("Login using registration credentials and get the token", async () => {
        const response = await auth.login("erikasypko@example.com", "password123");
        accessToken = response.body.token.accessToken.token;
    });

    it("should successfully log in and return a token", () => {
        expect(accessToken).to.be.a("string");
    });
});