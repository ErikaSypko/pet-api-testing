import { expect } from "chai";
import { PostsController } from "../lib/controllers/posts.controller";

describe("Posts controller", () => {
    let postsController;

    before(() => {
        postsController = new PostsController();
    });

    it("should return a list of posts", async () => {
        const response = await postsController.getAllPosts();

        expect(response.statusCode).to.equal(200);
        expect(response.body).to.be.an("array");
    });

    it("should create a new post", async () => {
        const postData = {
            authorId: 7111,
            previewImage: "preview.jpg",
            body: "This is the post content."
        };

        const accessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6IkpXVCJ9.eyJzdWIiOiJlcmlrYXN5cGtvMTEiLCJlbWFpbCI6ImVyaWthc3lwa29AZXhhbXBsZS5jb20iLCJqdGkiOiI2YzNjMjZlNy02OGNkLTQ2ZGYtYmJhMC03NzlhMDE2ZGYyZTAiLCJpYXQiOjE2OTE5NzExMjcsImlkIjoiNzExMSIsIm5iZiI6MTY5MTk3MTEyNiwiZXhwIjoxNjkxOTc4MzI2LCJpc3MiOiJUaHJlYWQuTkVUIFdlYkFQSSIsImF1ZCI6Imh0dHBzOi8vbG9jYWxob3N0OjQ0MzQ0In0.sLD9QJIS-4uCkkkf3Z8gE795I591O3B7uDGT90LPGks'

        const response = await postsController.createPost(postData, accessToken);

        expect(response.statusCode).to.equal(200); 
    });

    it("should like a post", async () => {
        const likeData = {
            entityId: 3758,
            isLike: true,
            userId: 7111
        };

        const accessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6IkpXVCJ9.eyJzdWIiOiJlcmlrYXN5cGtvMTEiLCJlbWFpbCI6ImVyaWthc3lwa29AZXhhbXBsZS5jb20iLCJqdGkiOiI2YzNjMjZlNy02OGNkLTQ2ZGYtYmJhMC03NzlhMDE2ZGYyZTAiLCJpYXQiOjE2OTE5NzExMjcsImlkIjoiNzExMSIsIm5iZiI6MTY5MTk3MTEyNiwiZXhwIjoxNjkxOTc4MzI2LCJpc3MiOiJUaHJlYWQuTkVUIFdlYkFQSSIsImF1ZCI6Imh0dHBzOi8vbG9jYWxob3N0OjQ0MzQ0In0.sLD9QJIS-4uCkkkf3Z8gE795I591O3B7uDGT90LPGks'

        const response = await postsController.likePost(likeData, accessToken);

        expect(response.statusCode).to.equal(200);
    });

    it("should return an error when trying to like a post without providing required data", async () => {
        const invalidLikeData = null; // Invalid data, missing required properties
    
        const accessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6IkpXVCJ9.eyJzdWIiOiJlcmlrYXN5cGtvMTEiLCJlbWFpbCI6ImVyaWthc3lwa29AZXhhbXBsZS5jb20iLCJqdGkiOiI2YzNjMjZlNy02OGNkLTQ2ZGYtYmJhMC03NzlhMDE2ZGYyZTAiLCJpYXQiOjE2OTE5NzExMjcsImlkIjoiNzExMSIsIm5iZiI6MTY5MTk3MTEyNiwiZXhwIjoxNjkxOTc4MzI2LCJpc3MiOiJUaHJlYWQuTkVUIFdlYkFQSSIsImF1ZCI6Imh0dHBzOi8vbG9jYWxob3N0OjQ0MzQ0In0.sLD9QJIS-4uCkkkf3Z8gE795I591O3B7uDGT90LPGks'

        const response = await postsController.likePost(invalidLikeData, accessToken);
    
        expect(response.statusCode).to.equal(400);
    });

    it("should return an error when trying to like a non-existent post", async () => {
        const invalidLikeData = {
            entityId: 9999, // Assuming this post ID doesn't exist
            isLike: true,
            userId: 7111
        };

        const accessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6IkpXVCJ9.eyJzdWIiOiJlcmlrYXN5cGtvMTEiLCJlbWFpbCI6ImVyaWthc3lwa29AZXhhbXBsZS5jb20iLCJqdGkiOiI2YzNjMjZlNy02OGNkLTQ2ZGYtYmJhMC03NzlhMDE2ZGYyZTAiLCJpYXQiOjE2OTE5NzExMjcsImlkIjoiNzExMSIsIm5iZiI6MTY5MTk3MTEyNiwiZXhwIjoxNjkxOTc4MzI2LCJpc3MiOiJUaHJlYWQuTkVUIFdlYkFQSSIsImF1ZCI6Imh0dHBzOi8vbG9jYWxob3N0OjQ0MzQ0In0.sLD9QJIS-4uCkkkf3Z8gE795I591O3B7uDGT90LPGks'
    
        const response = await postsController.likePost(invalidLikeData, accessToken);
    
        expect(response.statusCode).to.equal(404);
    });

    it("should return an error when trying to like a post with an invalid user ID", async () => {
        const invalidLikeData = {
            entityId: 3758,
            isLike: true,
            userId: "invalidUserId" // Invalid user ID format
        };
    
        const accessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6IkpXVCJ9.eyJzdWIiOiJlcmlrYXN5cGtvMTEiLCJlbWFpbCI6ImVyaWthc3lwa29AZXhhbXBsZS5jb20iLCJqdGkiOiI2YzNjMjZlNy02OGNkLTQ2ZGYtYmJhMC03NzlhMDE2ZGYyZTAiLCJpYXQiOjE2OTE5NzExMjcsImlkIjoiNzExMSIsIm5iZiI6MTY5MTk3MTEyNiwiZXhwIjoxNjkxOTc4MzI2LCJpc3MiOiJUaHJlYWQuTkVUIFdlYkFQSSIsImF1ZCI6Imh0dHBzOi8vbG9jYWxob3N0OjQ0MzQ0In0.sLD9QJIS-4uCkkkf3Z8gE795I591O3B7uDGT90LPGks'

        const response = await postsController.likePost(invalidLikeData, accessToken);
    
        expect(response.statusCode).to.equal(400);
    });
    
});